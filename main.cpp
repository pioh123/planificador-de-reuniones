#include <iostream>
#include <cstring>
#include <stdio.h>
#include <map>
#include <string>
#include <cstdlib>
#include <sstream>
using namespace std;

void recover(FILE *archivo, map<string,string>& mp)
{
    char str[80];
    while(feof(archivo)==0)
    {
        fscanf(archivo,"%[^\n]\n",str);
        string head,tail;
        char *ptr;
        ptr=strtok(str," ");
        head=ptr;
        while(ptr!=NULL)
        {
            tail =ptr;
            ptr=strtok(NULL,"\n");
        }
        mp[head]=tail;
    }
    map<string,string>::iterator it;
    for(it=mp.begin();it!=mp.end();it++)
        cout<<it->first<<"  "<<it->second<<endl;
}
void mapping(string &str, map<int,int> &mp)
{
    char * ch = new char[str.length()+1];
    int con=1;
    strcpy(ch,str.c_str());
    char *ca=strtok(ch," ,-,:");
    char *fi;
    int a,b,c,d;
    while(ca!=NULL)
    {
        if(con%2!=0)
        {
            fi=ca;
            a=atoi(fi);
        }
        else
        {
            if(con%4!=0)
            {
                fi=ca;
                b=a*60+atoi(fi);
            }
            else
            {
                fi=ca;
                c=a*60+atoi(fi);
                mp.insert(pair<int,int>(b,c));
            }
        }
        con++;
        ca=strtok(NULL," ,-,:");
    }
}
void compare(string &a, string &b,int valor)
{
    map<int,int>mp1,mp2,mp3;
    map<string,string>mp;
    mapping(a,mp1);
    mapping(b,mp2);
    int x,y;
    map<int,int>::iterator it1;
    map<int,int>::iterator it2;
    for(it1=mp1.begin();it1!=mp1.end();it1++)
    {
        for(it2=mp2.begin();it2!=mp2.end();it2++)
        {
            if(it1->first<it2->first && it1->second>it2->first || it1->first<it2->second && it1->second>it2->second)
            {
                if(it1->first<it2->first)
                    x=it2->first;
                else
                    x=it1->first;
                if(it1->second<it2->second)
                    y=it1->second;
                else
                    y=it2->second;
                if(y-x>=valor)
                    mp3.insert(pair<int,int>(x,y));
            }
        }
    }
    map<int,int>::iterator it;
    for(it=mp3.begin();it!=mp3.end();it++)
        cout<<it->first/60<<":"<<it->first%60<<"-"<<it->second/60<<":"<<it->second%60<<"  ";
    cout<<endl;
}
void find(map<string,string>& mp1, map<string,string>& mp2,int valor)
{
    map<string,string>::iterator it;
    cout<<"------Estos son los horarios disponibles para una reunion-----"<<endl<<endl;
    for(it=mp1.begin();it!=mp1.end();it++)
    {
        if(mp2.count(it->first))
        {
            string hr1,hr2;
            hr1=it->second;
            hr2=mp2.find(it->first)->second;

            cout<<it->first<<": ";
            compare(hr1,hr2,valor);
        }
    }
}
int main()
{
    FILE *archivo1=fopen("archivo.txt","r");
    map<string,string>a;
    recover(archivo1,a);
    cout<<"--------------"<<endl;
    FILE *archivo2=fopen("archivo1.txt","r");
    map<string,string>b;
    recover(archivo2,b);
    cout<<endl<<endl;
    int valor;
    cout<<"ingrese el valor en minutos para la reunion: ";
    cin>>valor;
    find(a,b,valor);
    return 0;
}
